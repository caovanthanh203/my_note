/**
 * Helper method @2018 by Cao Van Thanh
 */
// Plus 2 arg  
Handlebars.registerHelper('plus', function(arg1, arg2) {  
    return arg1 + arg2;  
});   

// v-if
Handlebars.registerHelper('v-if', function(condition, arg2) {  
    if (condition){
    	return arg2;
    }
    return '';
});  
// equal
Handlebars.registerHelper('equal', function(arg1, arg2) {  
	console.log(arg1, ':', arg2);
    return arg1 == arg2;
}); 
// v-for 
Handlebars.registerHelper('v-for', function(from, to, block) {  
    var accum = '';
    for(var i = from; i <= to; i++){
    	block.data.index = i;
    	accum += block.fn(i);
    }
    return accum;
});   

// pagiation alyz
Handlebars.registerHelper('pager', function(pagination, num) {  
    return viewPaging(pagination, num);
});   

function viewPaging(pagination, num) {
	let now = pagination.current_page;
	let total = pagination.last_page;
	let from = now;
    let to = now;
	let lr = num;
	let show_total = lr*2+1;
    if (total <= 0) return []; //ok
    from = from - lr; //0
    to = to + lr; //1
    if (from <= 1) { //when current page = 2
        from = 1;	//from = 1 [2]
        to = from + show_total; //to = 1 [2] 11 ok
    }
    if (to >= total) { //when last = 8 to = 10
        to = total; // 1 [2] 8
    }
    if (total <= show_total) { //ok 1 - 8
        to = total;
        from = 1;
    }
    if (to > now + lr) { //ok 1 - 8
        to = now + lr;
    }
    let arr = [];
    while (from <= to) {
        arr.push(from);
        from++;
    }
    console.log(arr);
    return arr;
}