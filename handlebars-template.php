<script id="table-content-template" type="text/x-handlebars-template">
@{{#each list as |value key|}}
<tr>
    <td>@{{plus (plus key 1) ../start_at}}</td>
    <td>@{{value.name}}</td> -->
    <td>@{{value.phone}}</td>
    <td class="text-center">@{{value.gender}}</td>
    <td class="text-center">
        <a onclick="listOrder(@{{value.id}})">
        @{{value.order_number}}
        </a>
    </td>
</tr>
@{{/each}}
</script>

<script id="pagination-content-template" type="text/x-handlebars-template">
        @{{#if pagination.total}}
        <ul class="pagination pagination-split">
            @{{#if (equal pagination.current_page 1)}}
                <li class="disabled">
                <a aria-label="Previous"><i class="fa fa-angle-left"></i></a>
                </li>
            @{{else}}
                <li>
                <a onclick="fetchData('@{{plus pagination.current_page -1}}')" aria-label="Previous"><i class="fa fa-angle-left"></i></a>
                </li>
            @{{/if}}
            @{{#each (pager pagination 4)}}
                @{{#if (equal ../pagination.current_page this)}}
                    <li class="active">
                    <a>@{{this}}</a>
                    </li>
                @{{else}}
                    <li>
                    <a onclick="fetchData('@{{this}}')">@{{this}}</a>
                    </li>
                @{{/if}}
            @{{/each}}
            @{{#if (equal pagination.last_page pagination.current_page)}}
                <li class="disabled">
                <a aria-label="Next"><i class="fa fa-angle-right"></i></a>
                </li>
            @{{else}}
                <li>
                <a onclick="fetchData('@{{plus pagination.current_page 1}}')" aria-label="Previous"><i class="fa fa-angle-right"></i></a>
                </li>
            @{{/if}}
        </ul>
        @{{/if}}
</script>