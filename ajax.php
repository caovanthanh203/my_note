	/**
	 * Full ajax catch with validation
	 */
	//ajax
	$.ajax(
	{
		type: 'POST',
		url: $("#add-form").attr("action"),
		data: new FormData($('#add-form')[0]),
		contentType: false,
		processData: false,
		contentType:false
	}
	).fail(
	function (err) 
	{
		document.getElementById('error_id').innerHTML = err.responseJSON.message.valid_message;
	}
	).done(
	function (data)
	{
		//To do when success
		swal(
		{
			title: data.message.title,
			text: data.message.text,
			type: "success",
		}
		).then(
		function () {
			location.reload();
		}
		);
	}
	)
	//error show
	<small class="help-block text-danger" id="error_id"></small>
	//remote
	$validator = Validator::make(request()->all(), [
		'title' => 'required|unique:categories'
		]);
	$title_valid = null;
	$title_valid = $validator->errors()->has('title')?true:false;
	if($validator->fails()){
		return $this->handleError(500, 
			[
			'title_error' => $title_valid,
			'valid_message' => $title_valid?$validator->errors()->first('title'):'',
			]);
	};

	//handlebar ajax search pagination remote
	public function fetchData()
	{   
    $page = Request()->page?Request()->page-1:0;
    $start_at = $page*15;
    $i = 0;
    $customerList = $this->customerRepo
    ->when(request()->filled('search_key'), function ($query) {
        return $query->where(function ($query) {
            return $query->where('name', 'LIKE', '%' . request('search_key') . '%')
            ->orWhere('phone', 'LIKE', '%' . request('search_key') . '%');
        });
    })
    ->whereHas('roles', function($query) {
        return $query->where('slug', 'user');
    })->setTransform(function ($customer) use ($i){
        $i++;
        return [
        'index' => $i,
        'id' => $customer->id,
        'recommender' => "#chuaco",
        'name' => $customer->name,
        'phone' => $customer->phone,
        'gender' => $customer->gender_name,
        'order_number' =>$customer->orders->count('id')
        ];
    })->paginate();
    return $this->handleSuccess(['list' => $customerList, 'start_at' => $start_at]);
	}
