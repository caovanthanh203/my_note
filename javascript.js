//remove utf  
function utfRemover(utf = ''){  
	utf = utf.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');  
	utf = utf.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');  
	utf = utf.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');  
	utf = utf.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');  
	utf = utf.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');  
	utf = utf.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');  
	ascii = utf.replace(/đ/gi, 'd');  
    //$('#inlineFormInputGroup').val(slug);  
    return ascii;  
};  
//slug maker  
function slugify(text)  
{  
	return text  
	.toString()  
	.toLowerCase()  
    	.replace(/\s+/g, '')            // Replace spaces with -  
    	.replace(/[^\w\-]+/g, '')       // Remove all non-word chars  
    	.replace(/\-\-+/g, '')         // Replace multiple - with single -  
    	.replace(/^-+/, '')             // Trim - from start of text  
    	.replace(/-+$/, '');            // Trim - from end of text  
    };  
});  
  
//real time binding  
$(document).ready(function() {  
	$('#input_id').bind("keyup paste", function(){  
		//to do anything  
	});  
});

$('#search').bind('keyup paste',function(){
    search_key = $(this).val();
    fetchData();
})

var search_key;
var context;
//render Hanle bar
function renderData(){
    var tableTemplate = $("#table-content-template").html();
    var paginationTemplate = $("#pagination-content-template").html();
    var theTableTemplate = Handlebars.compile(tableTemplate);
    var thePaginationTemplate = Handlebars.compile(paginationTemplate);
    var theTableCompiledHtml = theTableTemplate(context);
    var thePaginationCompiledHtml = thePaginationTemplate(context);
    $('#table-body').html(theTableCompiledHtml);
    $('#pagination-body').html(thePaginationCompiledHtml);
}

function fetchData(n = 1){
    $.ajax({   
        type : 'get',
        url : '{{route('admin.customer.list')}}',
        data: {
            'search_key': search_key,
            'page': n
        }
    }).fail(function (err) {
        //console.log(err);
        //document.getElementById('value_id').innerHTML = err.responseJSON.message.value_error_message;
    }).done(function (data){
        context = {
                "list": data.message.list.data, //"[{},{},{}]"
                "pagination": data.message.list,
                "start_at": data.message.start_at
            };
        renderData();
    })
}
