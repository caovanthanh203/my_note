#1. Full Ajax submit with Remote Custom Valiation message catch:

##1.1 Ajax
<script type="text/javascript">  
$.ajax({  
		//body  
	})  
	.fail(function (err)   
	{  
		//To do when err  
		document.getElementById('value_id').innerHTML = err.responseJSON.message.value_error_message;  
	}).done(function (data)  
	{  
		//To do when success  
		swal({  
            title: data.message.title,  
            text: data.message.text,  
            type: "success",  
            }).then(function () {  
            	location.reload();  
        });  
	})  
</script>  
##1.2 Error show  

<small class="help-block text-danger" id="error_id"></small> //to show error  

##1.3 Remote process  
<?php  
$validator = Validator::make(request()->all(),[  
		'xxx' => 'required'  
	]);   
	$boolean = null;  
	$boolean = $validator->errors()->has('xxx')?true:false;  
	if($validator->fails()){  
		return $this->handleError(500,   
			[  
			'is_xxx_error' => $title_valid,  
			'valid_message' => $title_valid?$validator->errors()->first('xxx'):'',  
			]);  
	};  
?>  
#2. Javascript  
##2.1 remove utf  
function utfRemover(utf = ''){  
	utf = utf.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');  
	utf = utf.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');  
	utf = utf.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');  
	utf = utf.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');  
	utf = utf.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');  
	utf = utf.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');  
	ascii = utf.replace(/đ/gi, 'd');  
    //$('#inlineFormInputGroup').val(slug);  
    return ascii;  
};  
##2.2 slug maker   
function slugify(text)  
{  
	return text  
	.toString()  
	.toLowerCase()  
    	.replace(/\s+/g, '')            // Replace spaces with -  
    	.replace(/[^\w\-]+/g, '')       // Remove all non-word chars  
    	.replace(/\-\-+/g, '')         // Replace multiple - with single -  
    	.replace(/^-+/, '')             // Trim - from start of text  
    	.replace(/-+$/, '');            // Trim - from end of text  
    };  
});  
  
##2.3 real time binding  
$(document).ready(function() {  
	$('#input_id').bind("keyup paste", function(){  
		//to do anything  
	});  
});

#3 HandleBar JS Pattern 
##3.1 Example target <p> with content get from ajax  
```html
<p id="target"><!-- handlebar will change this content--></p>
```
##3.2 Define a template for target content  
```html
<script id="p-content-template" type="text/x-handlebars-template">   
@{{content_tag}}  
</script>  
```
##3.3 Define render function and fetch function  
```javascript
var context;

function renderData(){
    var pTemplate = $("#p-content-template").html();
    var theTemplate = Handlebars.compile(tableTemplate);
    var theCompiledHtml = theTemplate(context);
    $('#target').html(theCompiledHtml);
}
function fetchData(){
    $.ajax({   
       		//body
        }
    }).fail(function (err) {
        //to do
    }).done(function (data){
        context = {
            "content_tag": //data from ajax
            };
        renderData(); //important to update content
    })
}
```
##3.4 Using  
*same as vueJs*  
```javascript
//when need to update
renderData();
```

#4 Handlebars Helper
##4.1 Plus  
{{plus 2 3}} // 3  

#5 Export Excel using excel 3.0  
##5.1 add Export class into models folder:  
/User  
-----/User.php  
-----/UserRepository.php  
-----/Export.php  

##5.2 /Export.php  
```php
namespace App\Models\User;  
use Maatwebsite\Excel\Concerns\FromCollection;  
use Maatwebsite\Excel\Concerns\WithHeadings;  
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;  

class Export implements FromCollection, WithHeadings, WithStrictNullComparison  
{  

	protected $repo;  
	protected $header;  

	public function headings(): array  
    {  
        return $this->header;  
    }  

	public function setRepo($repo){  
		$this->repo = $repo;  
	}  
  
	public function setHeader($header){  
		$this->header = $header;  
	}  

    public function collection()  
    {  
        return $this->repo->get();  
    }  
}  
```

##5.3 /UserRepository.php  
```php
use Maatwebsite\Excel\Facades\Excel;  
  
class UserRepository extends BaseRepository implements RepositoryInterface  
{
    public function model()  
    {  
        return User::class;  
    }  

    public function export($repo, $name, $header)  
    {  
    	$export = new Export;  
    	$export->setRepo($repo);  
    	$export->setHeader($header);  
    	return Excel::download($export, $name);  
    }   
}    
```

##5.4 using
<?php
		$header = ['#', 'Người giới thiệu', 'Họ và tên', 'SĐT', 'Giới tính', 'Số hóa đơn'];  
        $name = 'customerList-' . Carbon::now()->timestamp.'.xlsx';  //important  
        return $this->customerRepo->export($customerList, $name, $header);  
?>

##5.5 trigger in html  
<form action="url" id="exportForm">  
{{csrf_field()}}  
<input name="search">  
</form>  

function formSubmit() {  
    $('#exportForm').submit();  
}  

#6 Vias Table Template And Pagination for HandleBars
var table       = new ViasTable('table-body', 'table-content-template');
var pagination  = new ViasPagination('pagi-body');
function renderData(){
    table.refresh(context);
    pagination.refresh(context);
}

#7 Input money mask JS Mmoney Library

<input id="value_input" class="form-control" name="money_value" data-precision="0" placeholder="">

$(function() {
    $('#value_input').maskMoney();
})
