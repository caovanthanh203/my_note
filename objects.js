// function customTag(tagName,fn){
//   document.createElement(tagName);
//   //find all the tags occurrences (instances) in the document
//   var tagInstances = document.getElementsByTagName(tagName);
//         //for each occurrence run the associated function
//         for ( var i = 0; i < tagInstances.length; i++) {
//             fn(tagInstances[i]);
//         }
// }
// function codingdudeGravatar(element){
//         //code for rendering the element goes here
//             //get the email address from the element's email attribute
//             var gravatar = "https://images.pexels.com/photos/658687/pexels-photo-658687.jpeg?auto=compress&cs=tinysrgb&h=350";
//             element.innerHTML = "<img src='"+gravatar+"'>";
// }
// customTag("thanh",codingdudeGravatar);

class ViasPagination {
	constructor(container_id, fetcher_name = 'fetchData'){
		this.container_id = container_id;
		this.fetcher_name = fetcher_name;
		this.template = '{{#if pagination.total}}<ul class="pagination pagination-split"> {{#if (equal pagination.current_page 1)}}<li class="disabled"> <a aria-label="Previous"><i class="fa fa-angle-left"></i></a> </li>{{else}}<li> <a onclick="'+fetcher_name+'(\'{{plus pagination.current_page -1}}\')" aria-label="Previous"><i class="fa fa-angle-left"></i></a> </li>{{/if}}{{#each (pager pagination 4)}}{{#if (equal ../pagination.current_page this)}}<li class="active"> <a>{{this}}</a> </li>{{else}}<li> <a onclick="'+fetcher_name+'(\'{{this}}\')">{{this}}</a> </li>{{/if}}{{/each}}{{#if (equal pagination.last_page pagination.current_page)}}<li class="disabled"> <a aria-label="Next"><i class="fa fa-angle-right"></i></a> </li>{{else}}<li> <a onclick="'+fetcher_name+'(\'{{plus pagination.current_page 1}}\')" aria-label="Previous"><i class="fa fa-angle-right"></i></a> </li>{{/if}}</ul> {{/if}}';
		this.context = {};
		this.compiled = Handlebars.compile(this.template);
	}
	refresh(new_data){
		this.context = new_data;
		let html = this.compiled(this.context);
		$('#' + this.container_id).html(html);
	}
}

class ViasTable {
	constructor(container_id, new_template_id){
		this.container_id = container_id;
		this.template = $('#' + new_template_id).html();
		this.compiled = Handlebars.compile(this.template);
		this.context = {};
	}
	setTemplate(new_template_id){
		this.template = $('#' + new_template_id).html();;
		this.compiled = Handlebars.compile(this.template);
	}
	refresh(new_data){
		this.context = new_data;
		let html = this.compiled(this.context);
		$('#' + this.container_id).html(html);
	}
}